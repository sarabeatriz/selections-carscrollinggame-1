#include "play.h"
#include <ctime>


void play::setTrack(Track track_type){
    // This function is used to set the type of track allowed for
    // this game. The types of tracks are (play::DAY, play::NIGHT, play::BEACH,
    // play::CANDYLAND).
    // You will define a switch to select the track of the game, and
    // the obstacles according to the track_type received.
    // The tracks background and obstacles are:
    // For track type Day
    //      the obstacles are hole and cone.
    // For track type Night
    //      the obstacles are it or zombie
    // For track type Beach
    //      the obstacles are spongebob or patrick
    // For track type Candyland
    //      the obstacles is monster
    // To set the track background simply set the variable (track) to the path of
    // the background.
    // To set the obstacles, use the function setObstaclesPixmap()
    // For instance:   setObstaclesPixmap("cone") ;
}

bool play::obstacleCollision(Obstacle &obs, Car &car){
    // Complete the function using the selection structure if/else
    // to detect a collision between the obstacle and the car.
    // Recall from the instructions that the car does not move in the
    // X axis and that the fix X asix is stored in CARX.
    // OBSTACLERANGE contains the range from the obstacle center to
    // each side.


}


void play::flagCollision(Flag &flag, Car &car){
    // Complete the function using the selection structure if/else
    // to detect a collision between the flag and the car.
    // Recall from the instructions that the car does not move in the
    // X axis and that the fix X asix is stored in CARX.
    // FLAGRANGE contains the range from the flag center to
    // each side.
    // If there is a collision set the score to 30 more points
    // and hide the flag.


}
