#-------------------------------------------------
#
# Project created by QtCreator 2014-06-18T23:35:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GenericScrollingGame5
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    obstacle.cpp \
    flag.cpp \
    play.cpp \
    car.cpp \
    work.cpp

HEADERS  += mainwindow.h \
    obstacle.h \
    flag.h \
    play.h \
    car.h

FORMS    += mainwindow.ui

RESOURCES += \
    images.qrc

